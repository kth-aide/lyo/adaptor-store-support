/*******************************************************************************
 * Copyright (c) 2012, 2014 IBM Corporation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * * Contributors:
 *
 *    Hirotaka Matsumoto - Initial implementation
 *******************************************************************************/
package org.eclipse.lyo.oslc4j.bugzilla.trs;

import com.j2bugzilla.base.Bug;
import com.j2bugzilla.base.BugzillaConnector;
import com.j2bugzilla.base.BugzillaException;
import com.j2bugzilla.base.Product;
import com.j2bugzilla.rpc.BugSearch;
import com.j2bugzilla.rpc.GetProduct;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.eclipse.lyo.oslc4j.bugzilla.jbugzx.rpc.GetAccessibleProducts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.it.bcm.BugzillaCachedAdaptorManager;
import se.kth.md.it.bcm.servlet.ServletListener;

/**
 * This class represents the list of History data in Bugzilla
 */
public class ChangeBugzillaHistories {
    private final static Logger log = LoggerFactory.getLogger(ChangeBugzillaHistories.class);
    private static final SimpleDateFormat XSD_DATETIME_FORMAT;
    private static int MAX_BUG_COUNT;
    private static int MAX_PRODUCT_COUNT;
    private static Date STARTCHECKDATE;
    /**
     * Mutex
     */
    private static String mutex = "";
    /**
     * List of Base Resources (pageNum, List<Base>)
     */
    private static Date mostRecentChangeLogDate;

    /*
     * Added in Lab 1.3
     */
    private static Date lastBaseResourceUpdatedDate;
    private static long UPDATEINTERVAL;
    /**
     * Saved Bugzilla History
     */
    private static HistoryData[] prevBugHistories;

    static {
        // TODO: 2016-12-28 request settings from BugzillaCachedAdaptorManager
//        Properties props = new Properties();
        //props.load(BugzillaCachedAdaptorManager.class.getResourceAsStream
        // ("/test/resources/bugz.properties"));
//            String dir = System.getProperty("user.dir");
//            FileInputStream propertyFilePath = new FileInputStream(
//                dir + "/test/resources/bugz.properties");
//            props.load(propertyFilePath);
//            String numberOfBugs = props.getProperty("max_number_of_bugs");
//            if ((numberOfBugs != null) && (numberOfBugs.length() != 0)) {
//                MAX_BUG_COUNT = Integer.valueOf(numberOfBugs);
//            }
        if (MAX_BUG_COUNT == 0) {
            MAX_BUG_COUNT = 100;
        }
//            String numberOfProducts = props.getProperty("max_number_of_products");
//            if ((numberOfProducts != null) && (numberOfProducts.length() != 0)) {
//                MAX_PRODUCT_COUNT = Integer.valueOf(numberOfProducts);
//            }
        if (MAX_PRODUCT_COUNT == 0) {
            MAX_PRODUCT_COUNT = 5;
        }
    }

	/*
     * Retrieve the old implementation
	 */

    static {
        // TODO: 2016-12-28 request settings from BugzillaCachedAdaptorManager
//        Properties props = new Properties();
        //props.load(BugzillaCachedAdaptorManager.class.getResourceAsStream
        // ("/test/resources/bugz.properties"));
//            String dir = System.getProperty("user.dir");
//            FileInputStream propertyFilePath = new FileInputStream(
//                dir + "/test/resources/bugz.properties");
//            props.load(propertyFilePath);
//            String year_string = props.getProperty("start_date_year");
        int year = 0;
//            if ((year_string != null) && (year_string.length() != 0)) {
//                year = Integer.valueOf(year_string).intValue();
//            }
        if (year == 0) {
            year = 2013;
        }
//            String month_string = props.getProperty("start_date_month");
        int month = 0;
//            if ((month_string != null) && (month_string.length() != 0)) {
//                month = Integer.valueOf(month_string).intValue() - 1;
//            }
//            if (month == -1) {
//                month = 0;
//            }
//            String day_string = props.getProperty("start_date_day");
        int day = 0;
//            if ((day_string != null) && (day_string.length() != 0)) {
//                day = Integer.valueOf(day_string).intValue();
//            }
        if (day == 0) {
            day = 1;
        }
        STARTCHECKDATE = (new GregorianCalendar(year, month, day)).getTime();
    }

    static {
        XSD_DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        XSD_DATETIME_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    static {
        // TODO: 2016-12-28 request settings from BugzillaCachedAdaptorManager
//        Properties props = new Properties();
        //props.load(BugzillaCachedAdaptorManager.class.getResourceAsStream
        // ("/test/resources/bugz.properties"));
//            String dir = System.getProperty("user.dir");
//            FileInputStream propertyFilePath = new FileInputStream(
//                dir + "/test/resources/bugz.properties");
//            props.load(propertyFilePath);
//            String interval = props.getProperty("rebuild_interval");
//            if ((interval != null) && (interval.length() != 0)) {
//                UPDATEINTERVAL = Integer.valueOf(interval).longValue();
//            }
        if (UPDATEINTERVAL == 0) {
            UPDATEINTERVAL = 5 * 60 * 1000; // 5 min = 300s = 300000 milliseconds
        }
    }

    private ChangeBugzillaHistories() {
    }

    /**
     * Get a Bugzilla Bug History by id(and its Bug)
     * @return Bug
     */
    @Deprecated
    public static HistoryData[] getBugHistoryById(final HttpServletRequest request, final Bug bug,
        final String productIdString, final String bugIdString, final Date dayAfter)
        throws IOException, ServletException {
        final BugzillaConnector bc = BugzillaCachedAdaptorManager.getBugzillaConnector(request);
        return getBugHistoryById(bc, bug, productIdString, bugIdString, dayAfter);
    }

    public static HistoryData[] getBugHistoryById(final BugzillaConnector bc, final Bug bug,
        final String productIdString, final String bugIdString, final Date dayAfter)
        throws IOException, ServletException {
        try {
            int bugId = Integer.parseInt(bugIdString);
            // get time stamps for "modified" : Bugzilla history
            final GetHistory getHistory = new GetHistory(bugId);
            bc.executeMethod(getHistory);
            Date[] modifiedtimes_history = getHistory.getModifiedTimestamps(); // all is modified

            // get time stamps for "modified" : Bugzilla comments
            final GetComments getComments = new GetComments(bugId);
            bc.executeMethod(getComments);
            Date[] modifiedtimes_comments = getComments.getModifiedTimestamps(); // all is modified

            // get OSLC URI. See org.eclipse.lyo.oslc4j.bugzilla.services
            // .BugzillaChangeRequestService.createHtmlChangeRequest(String, String, String,
            // String, String, String, String)
            URI uri = new URI(
                ServletListener.getServletBase() + "/" + productIdString + "/changeRequests/" +
                    bugIdString);

            // get time stamp for "created"
            Date creationtime = (Date) (bug.getParameterMap().get("creation_time"));
            // TODO;
            // On my environment, Bugzilla returns wrong incorrect time ( It seems to be always
            // GMT... )
            //	    String x = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
            //		DateFormat.DEFAULT, Locale.getDefault()).format(o); // Looks like this seems
            // to be GMT time
            //	    o.toString();
            // build histories
            List<HistoryData> result = new ArrayList<HistoryData>(
                modifiedtimes_history.length + modifiedtimes_comments.length + 1);
            addModifiedResults(productIdString, dayAfter, bugId, modifiedtimes_history, uri,
                result);
            addModifiedResults(productIdString, dayAfter, bugId, modifiedtimes_comments, uri,
                result);
            if (dayAfter != null) {
                if (creationtime.compareTo(dayAfter) > 0) {
                    result.add(HistoryData.getInstance(Integer.parseInt(productIdString), bugId,
                        creationtime, uri, HistoryData.CREATED));
                }
            } else {
                result.add(
                    HistoryData.getInstance(Integer.parseInt(productIdString), bugId, creationtime,
                        uri, HistoryData.CREATED));
            }
            HistoryData[] histories = new HistoryData[result.size()];
            result.toArray(histories);
            return histories;
        } catch (Exception e) {
            System.out.println("PrintStackTrace for: " + bugIdString);
            e.printStackTrace();
            //JAD: it seems that some comments are causing the returned XML to be corrupt. For
            // this use case, just ignore such history.
            return new HistoryData[0];
            //throw new WebApplicationException(e);
        }
    }

    private static void addModifiedResults(final String productIdString, final Date dayAfter,
        final int bugId, final Date[] modifiedtimes_comments, final URI uri,
        final List<HistoryData> result) {
        for (Date t : modifiedtimes_comments) {
            if (dayAfter != null) {
                if (t.compareTo(dayAfter) > 0) {
                    result.add(
                        HistoryData.getInstance(Integer.parseInt(productIdString), bugId, t,
                            uri, HistoryData.MODIFIED));
                }
            } else {
                result.add(
                    HistoryData.getInstance(Integer.parseInt(productIdString), bugId, t, uri,
                        HistoryData.MODIFIED));
            }
        }
    }

    public static HistoryData[] getHistory(BugzillaConnector bc, Date dayAfter) {
        // See
        // org.eclipse.lyo.oslc4j.bugzilla.servlet.ServiceProviderCatalogSingleton
        // .initServiceProvidersFromProducts(HttpServletRequest)
        List<String> productIds = new ArrayList<>();
        if (bc == null) {
            return null;
        }
        GetAccessibleProducts getProductIds = new GetAccessibleProducts();
        try {
            bc.executeMethod(getProductIds);
        } catch (BugzillaException e) {
            log.error("Error executing GetAccessibleProducts() method", e);
            return null;  // TODO: 2016-12-28 bubble up
        }
        Integer[] ids = getProductIds.getIds();
        int numberOfProduct = 0;
        for (Integer p : ids) {
            productIds.add(Integer.toString(p));
            numberOfProduct++;
            if (numberOfProduct >= ChangeBugzillaHistories.MAX_PRODUCT_COUNT)
                break;
        }

        // get basePath
        // String basePath = BugzillaManager.getBugzServiceBase(); // this is
        // http://<hostname>:<port>/OSLC4JBugzilla/services

        // get bugs from each product
        List<HistoryData> allhistories = new ArrayList<>();
        if (dayAfter == null) {
            dayAfter = ChangeBugzillaHistories.STARTCHECKDATE;
        }
        for (String productid : productIds) {
            try {
                List<Bug> bugList = ChangeBugzillaHistories.getBugsByProduct(bc, productid,
                        /* page */
                    0, /* limit */ ChangeBugzillaHistories.MAX_BUG_COUNT, dayAfter);
                for (Bug bug : bugList) {
                    Collections.addAll(allhistories,
                        getBugHistoryById(bc, bug, productid, Integer.toString(bug.getID()),
                            dayAfter));
                }
            } catch (IOException | ServletException e) {
                log.error("Error accessing Bugzilla change histories", e);
                return null;  // TODO: 2016-12-28 bubble up
            }
        }
        allhistories.sort((object1, object2) -> {
            return object1.getTimestamp().compareTo(
                object2.getTimestamp()) * -1; // reverse sort, so new -> old
        });
        HistoryData[] result = new HistoryData[allhistories.size()];
        allhistories.toArray(result);
        return result;
    }

    private static List<Bug> getBugsByProduct(final BugzillaConnector bc,
        final String productIdString, int page, int limit, Date dayAfter)
        throws IOException, ServletException {
        List<Bug> results = null;
        try {
            int productId = Integer.parseInt(productIdString);
            final GetProduct getProducts = new GetProduct(productId);
            if (bc != null) {
                bc.executeMethod(getProducts);
                final Product product = getProducts.getProduct();
                final BugSearch bugSearch = ChangeBugzillaHistories.createBugSearch(page, limit,
                    product, dayAfter);
                log.trace("Fetching bugz by product #{}", productId);
                bc.executeMethod(bugSearch);
                results = bugSearch.getSearchResults();
            } else {
                System.err.println(
                    "Bugzilla Connector not initialized - check bugz.properties");
            }
        } catch (Exception e) {
            System.out.println("PrintStackTrace in method: " + "getBugsByProduct");
            e.printStackTrace();
            //JAD: it seems that some comments are causing the returned XML to be corrupt. For
            // this use case, just ignore such history.
            return null;
            //throw new WebApplicationException(e);
        }
        return results;
    }

    private static BugSearch createBugSearch(int page, int limit, Product product, Date dayAfter) {
        BugSearch.SearchQuery productQuery = new BugSearch.SearchQuery(
            BugSearch.SearchLimiter.PRODUCT, product.getName());
        BugSearch.SearchQuery limitQuery = new BugSearch.SearchQuery(BugSearch.SearchLimiter.LIMIT,
            (limit + 1) + "");
        BugSearch.SearchQuery offsetQuery = new BugSearch.SearchQuery(
            BugSearch.SearchLimiter.OFFSET, (page * limit) + "");
        if (dayAfter == null) {
            return new BugSearch(productQuery, limitQuery, offsetQuery);
        }
        BugSearch2 search = new BugSearch2(productQuery, limitQuery, offsetQuery);
        search.setDayAfter(new Date(dayAfter.getTime() + 1000)); // +1s : bugs dates > dayAfter
        return search;
    }

    /**
     * Create a list of Bugs for a product ID using paging
     * @return The list of bugs, paged if necessary
     */
    @Deprecated
    private static List<Bug> getBugsByProduct(final HttpServletRequest httpServletRequest,
        final String productIdString, int page, int limit, Date dayAfter)
        throws IOException, ServletException {
        List<Bug> results = null;
        try {
            final BugzillaConnector bc = BugzillaCachedAdaptorManager.getBugzillaConnector(
                httpServletRequest);
            final String pageString = httpServletRequest.getParameter("page");

            if (null != pageString) {
                page = Integer.parseInt(pageString);
            }
            int productId = Integer.parseInt(productIdString);
            final GetProduct getProducts = new GetProduct(productId);
            if (bc != null) {
                bc.executeMethod(getProducts);
                final Product product = getProducts.getProduct();
                final BugSearch bugSearch = ChangeBugzillaHistories.createBugSearch(page, limit,
                    product, dayAfter);
                bc.executeMethod(bugSearch);
                results = bugSearch.getSearchResults();
            } else {
                System.err.println(
                    "Bugzilla Connector not initialized - check bugz.properties");
            }
        } catch (Exception e) {
            System.out.println("PrintStackTrace in method: " + "getBugsByProduct");
            e.printStackTrace();
            //JAD: it seems that some comments are causing the returned XML to be corrupt. For
            // this use case, just ignore such history.
            return null;
            //throw new WebApplicationException(e);
        }
        return results;
    }

    private static HistoryData[] buildChangeLogsDiff(final BugzillaConnector bugzillaConnector) {
        return ChangeBugzillaHistories.getHistory(bugzillaConnector, mostRecentChangeLogDate);
    }

    /**
     * Build Bugzilla Base resources and ChangeLogs
     */
    @Deprecated
    private static void buildBaseResourcesAndChangeLogsInternal(
        HttpServletRequest httpServletRequest) throws URISyntaxException {
        BugzillaConnector bc = BugzillaCachedAdaptorManager.getBugzillaConnector(
            httpServletRequest);
        buildBaseResourcesAndChangeLogsInternal(bc);
    }

    private static void buildBaseResourcesAndChangeLogsInternal(
        final BugzillaConnector bugzillaConnector) {
        Date nowDate = new Date();
        if ((lastBaseResourceUpdatedDate != null) && (UPDATEINTERVAL != -1) && (nowDate.getTime()
            - lastBaseResourceUpdatedDate.getTime() > UPDATEINTERVAL)) {
            mostRecentChangeLogDate = null; // enforce to build all
        }
        boolean buildAll = ((mostRecentChangeLogDate == null));
        HistoryData[] updatedHistories = null;
        if (!buildAll) {
            // Get only updated Histories
            updatedHistories = ChangeBugzillaHistories.buildChangeLogsDiff(bugzillaConnector);
            if ((updatedHistories == null) || (updatedHistories.length == 0)) {
                return;
            }
        } else {
            System.out.println("Rebuild Base and ChangeLogs");
            mostRecentChangeLogDate = null;
            lastBaseResourceUpdatedDate = nowDate;
        }

        int basePagenum = 1;
        int changeLogPageNum = 1;

        // Try to get Bugzilla histories now.
        HistoryData[] histories = null;
        if (buildAll) {
            histories = ChangeBugzillaHistories.getHistory(bugzillaConnector, null);
            if (histories.length == 0) {
                return;
            }
        } else {
            int newSize = prevBugHistories != null ? prevBugHistories.length : 0;
            newSize += updatedHistories != null ? updatedHistories.length : 0;
            if (newSize == 0) {
                return;
            }
            histories = new HistoryData[newSize];
            int start = 0;
            if ((updatedHistories != null) && (updatedHistories.length > 0)) {
                System.arraycopy(updatedHistories, 0, histories, start, updatedHistories.length);
                start = updatedHistories.length;
            }
            if ((prevBugHistories != null) && (prevBugHistories.length > 0)) {
                System.arraycopy(prevBugHistories, 0, histories, start, prevBugHistories.length);
                start = prevBugHistories.length;
            }
        }
        prevBugHistories = new HistoryData[histories.length];
        System.arraycopy(histories, 0, prevBugHistories, 0, histories.length);
        //Jad: Irrespective whether we are to buildAll or not, prevBugHistories (= histories, but
        // it is a static variable which we can access) contains the histories so far

        int changeOrder = histories.length;
        int currentNumberOfMember = 0;
        int currentNumberOfChangeLog = 0;
        URI mostRecentChangeEventURI = null;
        List<URI> allmembers = buildAll ? new ArrayList<URI>(histories.length) : null;
        for (HistoryData historyData : histories) {
            URI uri = historyData.getUri();
            String changedUriTemplate = "urn:urn-3:" +
                "cm1.example.com" +
                ":" +
                XSD_DATETIME_FORMAT.format(historyData.getTimestamp()) + ":" +
                changeOrder;
            URI changedUri = URI.create(changedUriTemplate);
            if (mostRecentChangeEventURI == null) {
                mostRecentChangeEventURI = changedUri;
                mostRecentChangeLogDate = historyData.getTimestamp();
            }

            currentNumberOfChangeLog++;
            changeOrder--;
        }
    }

    public static void buildBaseResourcesAndChangeLogs(HttpServletRequest httpServletRequest)
        throws URISyntaxException {
        synchronized (mutex) {
            ChangeBugzillaHistories.buildBaseResourcesAndChangeLogsInternal(httpServletRequest);
        }
    }

    public static void buildBaseResourcesAndChangeLogs(final BugzillaConnector bugzillaConnector) {
        synchronized (mutex) {
            ChangeBugzillaHistories.buildBaseResourcesAndChangeLogsInternal(bugzillaConnector);
        }
    }

    public static HistoryData[] getChangeHistory() {
        return prevBugHistories;
    }

}
