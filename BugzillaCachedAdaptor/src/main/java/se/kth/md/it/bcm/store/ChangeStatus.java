/*******************************************************************************
 * Copyright (c) 2012, 2013 IBM Corporation.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 * 
 * * Contributors:
 * 
 *    Hirotaka Matsumoto - Initial implementation
 *******************************************************************************/
package se.kth.md.it.bcm.store;

import java.net.URI;
import java.util.Date;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;

/**
 * An instance of this class represents a change in a resource with URI uri,
 * which occurred at occurred.
 */
@Deprecated
public class ChangeStatus {
	final public static String CREATED = "Created";
	final public static String MODIFIED = "Modified";
	final public static String DELETED = "Deleted";

	protected URI uri;
	protected Date occured;
	protected String type = CREATED;
	protected AbstractResource resource;

	public ChangeStatus(URI uri, Date timestamp, String type, AbstractResource resource) {
		this.uri = uri;
		this.occured = timestamp;
		this.type = type;
		this.resource = resource;
		return;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeStatus other = (ChangeStatus) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	/**
	 * @return the timestamp
	 */
	public Date getOccured() {
		return occured;
	}

	/**
	 * @return the url
	 */
	public URI getUri() {
		return uri;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the type
	 */
	public AbstractResource getResource() {
		return resource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Type=" + getType() + ", " +
				"Occured=" + getOccured() + ", " +
				"URI=" + getUri() + "\n";
	}

}
