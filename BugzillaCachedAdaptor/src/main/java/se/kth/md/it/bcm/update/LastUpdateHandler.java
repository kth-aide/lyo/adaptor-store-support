package se.kth.md.it.bcm.update;

import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.Handler;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.HistoryResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LastUpdateHandler is .
 * @author Andrii Berezovskyi (andriib@kth.se)
 * @version $Id$
 * @since 0.0.0
 */
public class LastUpdateHandler<M> implements Handler<M> {
    private final static Logger log = LoggerFactory.getLogger(LastUpdateHandler.class);
    private final String storeUriKey;

    public LastUpdateHandler(String storeUriKey) {
        this.storeUriKey = storeUriKey;
    }

    @Override
    public void handle(final Store store, final Collection<Change<M>> changes,
        final Optional<M> message) {
        Optional<HistoryResource> resource = changes.stream()
            .filter(Objects::nonNull)
            .filter(mChange -> mChange.getHistoryResource() != null)
            .filter(mChange -> mChange.getHistoryResource().getTimestamp() != null)
            .map(Change::getHistoryResource)
            .max(Comparator.comparing(HistoryResource::getTimestamp));
        if (resource.isPresent()) {
            try {
                store.putResources(storeUriKey, resource.get());
            } catch (StoreAccessException e) {
                log.error("Cannot write the HistoryResource for last modification into key '{}'",
                    storeUriKey);
            }
        }
    }
}
